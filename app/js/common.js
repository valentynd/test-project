$( document ).ready(function() {   

// preloader
  $(".preloader").fadeOut();

var swiper = new Swiper('.swiper-container', {
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 30,
	autoplay: 4000,
  	loop: true,
	  fadeEffect: {
	    crossFade: true
	  }
});


});
